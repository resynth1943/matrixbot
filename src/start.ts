import { BootstrapService } from './services/bootstrap';
import {
  runInDomain,
  Domain,
  provide,
  useService,
  useKey
} from '@resynth1943/inject';
import { CommandService } from './services/command';
import { ConfigurationService } from './services/configuration';
import { ReactiveService } from './services/reactive';
import {
  CONFIGURATION_FILE_LOCATION,
  MATRIX_CLIENT,
  COMMAND_REGISTRY,
  COMMAND_PARSER_OPTIONS
} from './keys';
import { join as pathJoin } from 'path';
import { MatrixClient } from 'matrix-bot-sdk';
import { CommandParserService } from './services/command-parser';
import { ReplyService } from './services/reply';
import { commands } from './commands';

/** Creates an application domain. */
function createDomain(client: MatrixClient): Domain {
  return {
    services: [
      BootstrapService,
      CommandService,
      ConfigurationService,
      CommandParserService,
      ReactiveService,
      ReactiveService,
      ReplyService,
      ...commands
    ],
    providers: [
      provide(
        CONFIGURATION_FILE_LOCATION,
        pathJoin(process.cwd(), '/config.json')
      ),
      provide(MATRIX_CLIENT, client),
      provide(COMMAND_REGISTRY, new Map()),
      provide(COMMAND_PARSER_OPTIONS, { prefixes: ['$'] })
    ]
  };
}

function main(domain: Domain) {
  // Reeeeeeeeee let's fucking go!
  const reactiveService = useService(ReactiveService);
  const commandService = useService(CommandService);
  const commandRegistry = useKey(COMMAND_REGISTRY);

  // Time to add every command to the injected registry.
  for (const command of commands) {
    const { name } = command.getMetadata();
    commandRegistry.set(name, command);
  }

  // Boom! Now let's start listening for commands.
  reactiveService.createObservableFromCommands().subscribe({
    next: ({ target, command, context }) => {
      commandService.runCommand(command, target, context);
    }
  });
}

// Time to tap into the domain we just created, so we have a valid environment for the injector.
async function createClientAndStart() {
  // Request core services from the injector.
  const bootstrapService = useService(BootstrapService);
  const configurationService = useService(ConfigurationService);

  // Now it's time to access the configuration file.
  const {
    accessToken,
    homeserverURL
  } = await configurationService.getConfiguration();

  // We're using the memory storage for now. This is problematic for performance, as
  // each time the client starts, it has to perform an initial sync.
  const storage = bootstrapService.createDefaultStorage();

  // Now we've loaded the configuration, *and* have a working Matrix client.
  // All that's left to do is connect the client to Matrix.
  const client = bootstrapService.createClient({
    accessToken,
    homeserverURL,
    storage
  });

  // Base domain for the injector.
  const domain: Domain = createDomain(client);

  // When the cliet is fully started, we need to call 'main'.
  bootstrapService.startClient(client).subscribe({
    // Don't need to pass the domain in here, as that's implicit -- `useService / key` calls will
    // work in a domain context.
    next: () => runInDomain(domain, main)
  });
}

if (require.main === module) {
  createClientAndStart();
}
