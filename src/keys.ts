import { createKey } from '@resynth1943/inject';
import { MatrixClient } from 'matrix-bot-sdk';
import { BaseCommand } from './services/command';
import { CommandParserOptions } from './services/command-parser';

export const CONFIGURATION_FILE_LOCATION = createKey<string>(
  'io.resynth1943.matrix-bot.configuration-file-location'
);
export const MATRIX_CLIENT = createKey<MatrixClient>(
  'io.resynth1943.matrix-bot.matrix-client'
);
export const COMMAND_REGISTRY = createKey<Map<string, BaseCommand>>(
  'io.resynth1943.matrix-bot.command-registry'
);
export const COMMAND_PARSER_OPTIONS = createKey<CommandParserOptions>(
  'io.resynth1943.matrix-bot.command-parser-options'
);
