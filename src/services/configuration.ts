import { createService, useKey } from '@resynth1943/inject';
import { CONFIGURATION_FILE_LOCATION } from '../keys';

export const ConfigurationService = createService({
  // We mark this as async to allow any overriding services to do that too.
  // This creates an expectation that this function is async, even when the
  // actual implementation may not be.
  async getConfiguration() {
    const configurationFileLocation = useKey(CONFIGURATION_FILE_LOCATION);
    return require(configurationFileLocation);
  }
});
