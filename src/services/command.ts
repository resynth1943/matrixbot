import {
  createService,
  Service,
  useService,
  useKey
} from '@resynth1943/inject';
import { MessageEvent, MessageEventContent } from 'matrix-bot-sdk';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageObservableEvent } from './reactive';
import { COMMAND_REGISTRY } from '../keys';
import { ExtractedCommand } from './command-parser';

/** Defines the metadata of a specific command. This is for documentational / command purposes. */
export interface BaseCommandMetadata {
  name: string;
  description: string;
}

export interface BaseCommandContext {
  roomId: string;
}

// This type is here just as a shorthand, compare the two:
// MessageEvent<MessageEventContent>
// BaseCommandMessage
// The former is quite tedious to type.
/** Alias for `MessageEvent<MessageEventContent>`. */
export interface BaseCommandMessage extends MessageEvent<MessageEventContent> {}

export interface BaseCommandTarget extends ExtractedCommand {
  message: BaseCommandMessage;
}

/** Defines a command intended to be passed to the command service (bound to a name). */
export interface BaseCommand extends Service {
  getMetadata(): BaseCommandMetadata;
  run(
    target: BaseCommandTarget,
    context: BaseCommandContext
  ): void | Promise<void>;
}

export const CommandService = createService({
  getCommandByName(name: string): BaseCommand | void {
    const commandRegistry = useKey(COMMAND_REGISTRY);
    return commandRegistry.get(name);
  },

  hasCommandByName(name: string) {
    const commandRegistry = useKey(COMMAND_REGISTRY);
    return commandRegistry.has(name);
  },

  deleteCommandByName(name: string) {
    const commandRegistry = useKey(COMMAND_REGISTRY);
    commandRegistry.delete(name);
  },

  runCommandByName(
    name: string,
    target: BaseCommandTarget,
    context: BaseCommandContext
  ) {
    const commandService = useService(CommandService);
    if (!commandService.hasCommandByName(name)) {
      // Command doesn't exist, nothing to run.
      return false;
    }

    const command = commandService.getCommandByName(name);

    if (command) {
      return command.run(target, context);
    }
  },

  runCommand(
    command: BaseCommand,
    target: BaseCommandTarget,
    context: BaseCommandContext
  ) {
    return command.run(target, context);
  },

  setCommandByName(name: string, command: BaseCommand) {
    const commandService = useService(CommandService);
    const commandRegistry = useKey(COMMAND_REGISTRY);

    if (commandService.hasCommandByName(name)) {
      // If the command already exists, return `false` as a safety mechanism.
      // Of course, commands can be overriden, but one must call `deleteCommandByName`
      // prior to replacing it in the registry.
      // This prevents double writes, and ensures the developer has the intent to replace
      // the command.
      return false;
    }

    commandRegistry.set(name, command);
    return true;
  },

  /** Maps a `MessageObservableEvent` Observable to `{ ...MessageObservableEvent, context: BaseCommandContext }`. */
  mapMessageObservableWithContext(
    messageObserver: Observable<MessageObservableEvent>
  ) {
    return messageObserver.pipe(
      map(object => {
        const commandService = useService(CommandService);
        const context = commandService.createCommandContext({
          roomId: object.roomID
        });
        return { ...object, context };
      })
    );
  },

  createCommandContext(context: BaseCommandContext) {
    return context;
  }
});
