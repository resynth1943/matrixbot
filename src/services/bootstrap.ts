import {
  MatrixClient,
  IStorageProvider,
  MemoryStorageProvider
} from 'matrix-bot-sdk';
import { createService } from '@resynth1943/inject';
import { from } from 'rxjs';

export interface CreateClientOptions {
  accessToken: string;
  homeserverURL: string;
  storage: IStorageProvider;
}

// !!! This service MUST not depend on any other services, and MUST not assume it is being used in a domain execution
// !!! context. This is because of a circular dependency with MATRIX_CLIENT in start.ts
// !!! (BootstrapService *should* create the client in a domain context, and we need to place the client in the domain *to get* a domain...)
export const BootstrapService = createService({
  /** Create a MatrixClient with the passed configuration values. */
  createClient({ accessToken, homeserverURL, storage }: CreateClientOptions) {
    return new MatrixClient(homeserverURL, accessToken, storage);
  },

  /** Create a storage mechanism for the client. By default, this is a memory storage provider. */
  createDefaultStorage() {
    // Remember that using a memory storage provider is definitely not recommended in production.
    // For example, on every start, the client will *have* to pull an initial sync from Matrix,
    // which may be extremely slow.
    // Other storage mechanisms (like the file-system one) only do this once, and store the initial
    // sync data in the file-system.
    // We should definitely use that in production instead.
    return new MemoryStorageProvider();
  },

  /** Start the MatrixClient's connection to Matrix. */
  startClient(client: MatrixClient) {
    return from(client.start());
  },

  stopClient(client: MatrixClient) {
    // This doesn't return a promise, so just return a null Observable.
    client.stop();
    return from([]);
  }
});
