import { createService, useKey, useService } from '@resynth1943/inject';
import { RichReply, MatrixEvent } from 'matrix-bot-sdk';
import { BaseCommandContext } from './command';
import { MATRIX_CLIENT } from '../keys';
import { from } from 'rxjs';

export type MessageMarkupType = 'html' | 'plain';

interface ReplyToMessageOptions {
  /** The content to reply with. */
  content: string;

  /** The context in which to reply to the target event. */
  context: BaseCommandContext;

  /** The event to reply to. */
  targetEvent: MatrixEvent<any>;

  /** The HTML content to reply with. This is added to `formatted_body`. */
  htmlContent?: string;
}

export const ReplyService = createService({
  /** Create a reply event relevant to the passed parameters. */
  createReplyForEvent({
    content,
    context,
    targetEvent,
    htmlContent = ''
  }: ReplyToMessageOptions): RichReplyReturnType {
    const { roomId } = context;
    return RichReply.createFor(roomId, targetEvent, content, htmlContent);
  },

  /** Reply to an event. */
  replyToEvent(reply: RichReplyReturnType, context: BaseCommandContext) {
    const matrixClient = useKey(MATRIX_CLIENT);
    return from(matrixClient.sendMessage(context.roomId, reply));
  },

  replyToEventImmediate(replyOptions: ReplyToMessageOptions) {
    const replyService = useService(ReplyService);

    const reply = replyService.createReplyForEvent(replyOptions);
    return replyService.replyToEvent(reply, replyOptions.context);
  }
});

// Bit hacky because `RichReply.createFor`'s return type isn't *actually* documented, so I'm just making do here.
// This type declaration should break if `RichReply.createFor` includes a valid type declaration and changes an interface.
// Can't be waiting for other people to do it, I'll do it :D
/** Describes the return type of `RichReply.createFor`. */
export interface RichReplyReturnType {
  'm.relates_to': {
    'm.in_reply_to': {
      event_id: string;
    };
  };
  msgtype: string;
  body: string;
  format: string;
  formatted_body: string;
}
