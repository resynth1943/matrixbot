import { createService, useService } from '@resynth1943/inject';

export interface CommandParserOptions {
  prefixes: string[];
}

export interface ExtractedCommand {
  arguments: string[];
  prefix: string;
  name: string;
}

export const CommandParserService = createService({
  // This method exists to prevent a circular dependency between extractArguments -> extractFlags.
  baseExtractArguments(content: string) {
    return content.split(' ').slice(1);
  },

  /** Extract any prefix included in `content`, *and* included as a prefix in `options`. */
  extractPrefix(content: string, options: CommandParserOptions) {
    return options.prefixes.find(prefix => content.startsWith(prefix));
  },

  /** Extract the command name from the specified content string. */
  extractCommandName(content: string, options: CommandParserOptions) {
    const commandParserService = useService(CommandParserService);
    const prefix = commandParserService.extractPrefix(content, options);

    if (prefix) {
      return content.slice(prefix.length).split(' ')[0];
    }
  },

  /** Extract the arguments from the specified content string. */
  extractArguments(content: string) {
    return content.split(' ').slice(1);
  },

  /**
   * This is the central function for parsing the content.
   * ```ts
   * const commandParserService = useService(CommandParserService);
   * const options = { prefixes: ['hey '] };
   *
   * const result = commandParser.extractAll('hey command_name argument --flag', options);
   * ```
   *
   * This will result in this JSON object, which represents the parsed fragments of the content:
   *
   * ```json
   * {
   *   prefix: 'hey',
   *   name: 'command_name',
   *   arguments: ['argument']
   * }
   * ```
   */
  extractAll(content: string, options: CommandParserOptions): ExtractedCommand | void {
    const commandParserService = useService(CommandParserService);

    const prefix = commandParserService.extractPrefix(content, options);
    const name = commandParserService.extractCommandName(content, options);

    if (!prefix || !name) {
      return;
    }

    return {
      arguments: commandParserService.extractArguments(content),
      prefix,
      name
    };
  }
});
