import { createService, useService, useKey } from '@resynth1943/inject';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import {
  BaseCommandMessage,
  CommandService,
  BaseCommand,
  BaseCommandContext,
  BaseCommandTarget
} from './command';
import { isValidEvent } from '../shared/message-filtration';
import { CommandParserService, ExtractedCommand } from './command-parser';
import { COMMAND_PARSER_OPTIONS, MATRIX_CLIENT } from '../keys';

/** An event from a stream created from `createObservableFromMessageEvents`. */
export interface MessageObservableEvent {
  eventName: 'm.room.message';
  roomID: string;
  message: BaseCommandMessage;
}

/** An event from a stream created from `createObservableFromMessageEvents`. */
export interface CommandObservableEvent extends MessageObservableEvent {
  context: BaseCommandContext;
  target: BaseCommandTarget;
  extracted?: ExtractedCommand;
  command: BaseCommand;
}

export const ReactiveService = createService({
  /** Creates an observable which streams all events of a specific type, emitted from the specified Matrix client. */
  createObservableFromEvent(
    event: string
  ): Observable<{ event: string; data: unknown[] }> {
    const matrixClient = useKey(MATRIX_CLIENT);

    return new Observable(observer => {
      const handler = (...data: unknown[]) => observer.next({ event, data });
      matrixClient.on(event, handler);

      // When the observer has disconnected, we don't need the handler. Let's remove it.
      return () => matrixClient.off(event, handler);
    });
  },

  /** Creates an observable which streams Message Events, emitted from the specified Matrix client. */
  createObservableFromMessageEvents(): Observable<MessageObservableEvent> {
    const reactiveService = useService(ReactiveService);
    const matrixClient = useKey(MATRIX_CLIENT);

    // We cast here as 'm.room.message' means nothing to the Matrix library (no types bound to event types yet).
    // This is definitely safe.
    return reactiveService.createObservableFromEvent('room.message').pipe(
      map(
        // Change around some of the data so it's easier to access.
        // The cast here is 100% safe.
        ({ event: eventName, data: [roomID, message] }: any) => ({
          eventName,
          roomID,
          message
        })
      )
    );
  },

  createObservableFromCommands() {
    const reactiveService = useService(ReactiveService);
    const commandParserService = useService(CommandParserService);
    const commandService = useService(CommandService);
    const commandParserOptions = useKey(COMMAND_PARSER_OPTIONS);

    // Create a message stream.
    const messageEvents$ = reactiveService.createObservableFromMessageEvents();

    return messageEvents$.pipe(
      /* Ensure all passed events are actually valid, fitting this criteria: 
        - Has a 'content' field, which has a type of `string`
        - The type of message is textual; we make sure `event.content.msgtype` is equal to "m.text"
        - The event has a `body` field
        This is, of course, composed into the `isValidEvent` function.
      */
      filter(({ message }) => isValidEvent(message)),

      map(object => ({
        ...object,
        // Extract the arguments, prefix and content from the content of the message event.
        extracted: commandParserService.extractAll(
          (object.message.content as any) as string,
          commandParserOptions
        )
      })),

      filter(({ extracted }) => {
        // Filter out any invalid command names (and non-commands).
        if (extracted === undefined) {
          return false;
        }

        return commandService.hasCommandByName(extracted.name);
      }),

      map(({ extracted, message, roomID }) => {
        const target = { ...(extracted as ExtractedCommand), message };
        const context = { roomId: roomID }; // todo: change roomID to roomId in declaration for consistence.

        /* Now we're putting it all together -- it's time to pass all the data onto the subscriber.
           We've turned an `Observable<MessageObservableEvent>` into an `Observable<CommandObservableEvent>`. */
        return {
          extracted,
          target,
          // The cast is safe because we checked the command's legitimacy in the previous filter pipe, so the cast is safe.
          command: commandService.getCommandByName(
            // The cast is safe. We ensure `extracted` is available above.
            (extracted as ExtractedCommand).name as string
          ) as BaseCommand,
          context
        };
      })
    );
  }
});
