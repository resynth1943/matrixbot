import { createService, useService } from '@resynth1943/inject';
import {
  BaseCommand,
  BaseCommandContext,
  BaseCommandTarget
} from '../services/command';
import { ReplyService } from '../services/reply';

export const PingCommandService: BaseCommand = createService({
  getMetadata() {
    return {
      name: 'ping',
      description: 'Test the connection of this bot to Matrix.'
    };
  },

  run({ message }: BaseCommandTarget, context: BaseCommandContext) {
    const replyService = useService(ReplyService);

    replyService.replyToEventImmediate({
      content: 'Ping!',
      context,
      targetEvent: message
    });
  }
});
