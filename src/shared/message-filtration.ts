import { MatrixEvent } from 'matrix-bot-sdk';
import { hasOwnProperty } from './util';
import R from 'ramda';

/** Ensure the event has a `content` field. */
export function eventHasContent(event: MatrixEvent<any>) {
  return hasOwnProperty(event, 'content') && event.content !== '';
}

/** Ensure the type of message event is textual; ensure `event.content.msgtype` is equal to "m.text". */
export function isTextEvent(event: MatrixEvent<any>) {
  return eventHasContent(event) && (event.content as any).msgtype === 'm.text';
}

/** Ensure the event has a `body` field. */
export function eventHasBody(event: MatrixEvent<any>) {
  return eventHasContent(event) && Boolean((event.content as any).body);
}

/* Ensure the passed event is actually valid, fitting this criteria: 
    - Has a 'content' field, which has a type of `string`
    - The type of message is textual; we make sure `event.content.msgtype` is equal to "m.text"
    - The event has a `body` field
*/
export const isValidEvent = R.allPass([
  eventHasContent,
  isTextEvent,
  eventHasBody
]);
