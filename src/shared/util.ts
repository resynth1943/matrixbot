// Store `hasOwnProperty` now, so later reassignments won't mess this up.
const __hasOwnProperty = Object.prototype.hasOwnProperty;

export function hasOwnProperty(object: unknown, key: unknown) {
  return __hasOwnProperty.call(object, key as any);
}
