# resynth1943.matrix-bot

A bot for [Matrix](https://matrix.org).
Written in TypeScript; uses Dependency Injection; easily extensible with custom commands and functions.

## License

This code is licensed under the [GPL 2.0 or later](https://codeberg.org/resynth1943/matrixbot/src/branch/master/LICENSE) license.
Thanks to gnu.org for creating the GPL.